#ifndef AGL_COMPOSITOR_H
#define AGL_COMPOSITOR_H

#include <wayland-client.h>

#include "agl-shell-client-protocol.h"
#include "agl-shell-desktop-client-protocol.h"

struct display_data;

struct display_data *
agl_shell_init(void);

struct wl_display *
agl_shell_get_wayland_display(struct display_data *display_data);

int
agl_shell_should_bind(struct display_data *display_data,
		      const char *if_name, uint32_t ver);
void
agl_shell_terminate(struct display_data *display_data);

int
agl_shell_setup(struct display_data *display_data);

/*
 * assumes one has called shell_desktop_install_listener()
 *
 * shell_desktop_add_listener() is called when binding to the
 */
void
shell_desktop_add_listener(struct display_data *data);

/*
 * shell_desktop_install_listener() should be followed by
 * shell_desktop_add_listener(), or should be called before agl_shell_setup()
 */
void
shell_desktop_install_listener(struct display_data *data,
			       const struct agl_shell_desktop_listener *listener,
			       void *user_data);
int
agl_shell_display_dispatch(struct display_data *display_data);

#endif
