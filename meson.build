project('libagl-compositor',
  'c',
  version: '0.0.18',
  default_options: [
    'warning_level=3',
    'c_std=gnu99',
  ],
  meson_version: '>= 0.47',
  license: 'MIT/Expat',
)

config_h = configuration_data()
pkgconfig = import('pkgconfig')
libaglversion = '0.0.18'
env_modmap = ''

cc = meson.get_compiler('c')
add_project_arguments(
  cc.get_supported_arguments([
    '-Wno-unused-parameter',
    '-Wno-pedantic',
    '-Wextra',
    '-Werror'
  ]),
  language: 'c'
)

add_project_arguments([
    '-DPACKAGE_STRING="libagl-compositor @0@"'.format(meson.project_version()),
    '-D_GNU_SOURCE',
    '-D_ALL_SOURCE',
  ],
  language: 'c'
)

common_inc = [ include_directories('.'), '../include/' ]
dep_scanner = dependency('wayland-scanner', native: true)
prog_scanner = find_program(dep_scanner.get_pkgconfig_variable('wayland_scanner'))
dep_wp = dependency('agl-compositor-0.0.18-protocols', version: '>= 0.0.18')
dir_wp_base = dep_wp.get_pkgconfig_variable('pkgdatadir')

protocols = [
  { 'name': 'agl-shell', 'source': 'external' },
  { 'name': 'agl-shell-desktop', 'source': 'external' },
]

foreach proto: protocols
    if proto['source'] == 'external'
        base_file = proto['name']
	xml_path = join_paths(dir_wp_base, '@0@.xml'.format(base_file))
    endif

    foreach output_type: [ 'client-header', 'server-header', 'private-code' ]
	if output_type == 'client-header'
	    output_file = '@0@-client-protocol.h'.format(base_file)
	elif output_type == 'server-header'
	    output_file = '@0@-server-protocol.h'.format(base_file)
	else
	    output_file = '@0@-protocol.c'.format(base_file)
	    if dep_scanner.version().version_compare('< 1.14.91')
	        output_type = 'code'
	    endif
	endif

	var_name = output_file.underscorify()
	target = custom_target(
	    '@0@ @1@'.format(base_file, output_type),
	    command: [ prog_scanner, output_type, '@INPUT@', '@OUTPUT@' ],
	    input: xml_path,
	    output: output_file,
	)

        set_variable(var_name, target)
    endforeach
endforeach

deps_wayland = [
  dependency('wayland-client'),
]

dir_prefix = get_option('prefix')
dir_lib = join_paths(dir_prefix, get_option('libdir'))

configure_file(output: 'config.h', configuration: config_h)

srcs_libagl_compositor = [
	'libagl-compositor',
	'src/shell.c',
	agl_shell_client_protocol_h,
	agl_shell_desktop_client_protocol_h,
	agl_shell_protocol_c,
	agl_shell_desktop_protocol_c,
]

libagl = shared_library(
        srcs_libagl_compositor,
	include_directories: common_inc,
	dependencies: deps_wayland,
	name_prefix: '',
	version: libaglversion,
	install: true,
	install_dir: dir_lib,
)

dep_libagl = declare_dependency(link_with: libagl)
deps_for_libagl_users = [ deps_wayland ]

pkgconfig.generate(
	libagl,
        filebase: 'libagl-compositor-@0@'.format(libaglversion),
        name: 'libagl-compositor',
        version: libaglversion,
        description: 'headers for agl-compositor library',
        requires_private: deps_for_libagl_users,
        subdirs: 'include'
)


env_modmap += 'libagl-compositor.so=@0@;'.format(libagl.full_path())

subdir('include')
subdir('example')
