#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include <agl-compositor.h>

struct data_container {
	struct display_data *dd;
};

static int running = 0;

static void
application_id(void *data, struct agl_shell_desktop *agl_shell_desktop,
	       const char *app_id)
{
	struct data_container *container = data;
	(void) container;

	fprintf(stdout, "received application_id event\n");
}

static void
application_id_state(void *data, struct agl_shell_desktop *agl_shell_desktop,
		     const char *app_id, const char *app_data,
		     uint32_t app_state, uint32_t app_role)
{
	struct data_container *container = data;

	(void) app_data;
	(void) agl_shell_desktop;
	(void) app_id;
	(void) app_state;
	(void) app_role;
	(void) container;

	fprintf(stdout, "received application_id_state event\n");
}

static const struct agl_shell_desktop_listener desktop_listener = {
	application_id,
	application_id_state,
};

static void
signal_int(int sig, siginfo_t *si, void *unused)
{
	running = 0;
}

int main(void)
{
	int ret;
	struct data_container container = {};
	struct sigaction sa;

	sa.sa_sigaction = signal_int;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESETHAND | SA_SIGINFO;
	sigaction(SIGINT, &sa, NULL);

	container.dd =  agl_shell_init();
	if (!container.dd)
		return -1;

	/* agl-shell is reserverd for HMI */
	/* ret = agl_shell_should_bind(container.dd, "agl_shell", 1); */

	ret = agl_shell_should_bind(container.dd, "agl_shell_desktop", 1);
	if (ret)
		return ret;

	shell_desktop_install_listener(container.dd, &desktop_listener, &container);

	/* connect and bind to the specified interfaces */
	agl_shell_setup(container.dd);

	running = 1;
	while (running && ret != -1)
		ret = agl_shell_display_dispatch(container.dd);

	agl_shell_terminate(container.dd);
	fprintf(stdout, "example, exiting\n");
	return 0;
}
