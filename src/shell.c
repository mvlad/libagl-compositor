#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "zalloc.h"

#include <agl-compositor.h>

struct shell_interface {
	char *name;
	uint32_t ver;
	struct wl_list link;	/** display_data::interfaces */	
};

struct display_data {
	struct wl_display *display;
	struct wl_registry *registry;

	struct agl_shell *shell;
	struct agl_shell_desktop *shell_desktop;

	const struct agl_shell_desktop_listener *desktop_shell_listener;
	void *desktop_shell_user_data;

	struct wl_list interfaces;
};

static void
global_add(void *data, struct wl_registry *reg, uint32_t name,
	   const char *iface_name, uint32_t version)
{
	struct display_data *dd = data;
	struct shell_interface *interface;
	bool to_bind = false;

	wl_list_for_each(interface, &dd->interfaces, link) {
		if (strcmp(iface_name, interface->name) == 0) {
			to_bind = true;
			break;
		}
	}

	if (!to_bind)
		return;

	if (strcmp(iface_name, agl_shell_interface.name) == 0) {
		dd->shell =
			wl_registry_bind(reg, name, &agl_shell_interface, 1);
	} else if (strcmp(iface_name, agl_shell_desktop_interface.name) == 0) {
		dd->shell_desktop =
			wl_registry_bind(reg, name,
					 &agl_shell_desktop_interface, 1);

		// install event handlers if we have found one installed,
		// as this assumes that 
		shell_desktop_add_listener(dd);
	}
}

static void
global_remove(void *data, struct wl_registry *reg, uint32_t id)
{
	(void) data;
	(void) reg;
	(void) id;
}

static const struct wl_registry_listener registry_listener = {
	global_add,
	global_remove,
};

struct display_data *
agl_shell_init(void)
{
	struct display_data *display_data = zalloc(sizeof(*display_data));

	if (!display_data)
		return NULL;

	display_data->display = wl_display_connect(NULL);
	if (!display_data->display) {
		free(display_data);
		return NULL;
	}

	wl_list_init(&display_data->interfaces);
	return display_data;
}

struct wl_display *
agl_shell_get_wayland_display(struct display_data *display_data)
{
	return display_data->display;
}

int
agl_shell_should_bind(struct display_data *display_data,
		      const char *if_name, uint32_t ver)
{
	assert(display_data->display != NULL);

	struct shell_interface *shell_if = zalloc(sizeof(*shell_if));

	if (!shell_if)
		return -1;

	shell_if->name = strdup(if_name);
	shell_if->ver = ver;

	wl_list_insert(&display_data->interfaces, &shell_if->link);

	return 0;
}

void
agl_shell_should_destroy(struct display_data *display_data, const char *if_name)
{
	struct shell_interface *interface;
	struct shell_interface *interface_tmp;

	wl_list_for_each_safe(interface, interface_tmp, &display_data->interfaces, link) {
		if (strcmp(if_name, interface->name) == 0) {
			wl_list_remove(&interface->link);
			free(interface->name);
			free(interface);
			break;
		}
	}
}

void
agl_shell_terminate(struct display_data *display_data)
{
	struct shell_interface *interface;
	struct shell_interface *interface_tmp;

	wl_list_for_each_safe(interface, interface_tmp,
			      &display_data->interfaces, link) {
		wl_list_remove(&interface->link);
		free(interface->name);
		free(interface);
	}

	if (display_data->shell)
		agl_shell_destroy(display_data->shell);

	if (display_data->shell_desktop)
		agl_shell_desktop_destroy(display_data->shell_desktop);

	wl_registry_destroy(display_data->registry);
	wl_display_flush(display_data->display);
	wl_display_disconnect(display_data->display);

	free(display_data);
}

void
shell_desktop_add_listener(struct display_data *data)
{
	fprintf(stdout, "shell_desktop_add_listener()\n");

	if (data->desktop_shell_listener) {
		fprintf(stdout, "adding listener\n");
		agl_shell_desktop_add_listener(data->shell_desktop,
					       data->desktop_shell_listener,
					       data->desktop_shell_user_data);
	}
}

void
shell_desktop_install_listener(struct display_data *data,
			       const struct agl_shell_desktop_listener *listener, void *user_data)
{
	assert(data->desktop_shell_listener == NULL);

	data->desktop_shell_listener = listener;
	data->desktop_shell_user_data = user_data;
}

/**
 *
 */
int
agl_shell_setup(struct display_data *display_data)
{
	struct wl_registry *registry;

	assert(display_data->display != NULL);
	registry = wl_display_get_registry(display_data->display);
	if (!registry)
		return -1;

	wl_registry_add_listener(registry, &registry_listener, display_data);
	wl_display_roundtrip(display_data->display);

	display_data->registry = registry;

	return 0;
}

int
agl_shell_display_dispatch(struct display_data *display_data)
{
	return wl_display_dispatch(display_data->display);
}

/*
 * struct display_data dd = {};
 * int ret;
 * if (agl_shell_init(&dd))
 * 	return -1;
 *
 * ret = agl_shell_should_bind(&dd, agl_shell_interface.name, 1);
 * if (ret)
 * 	return ret;
 * ret = agl_shell_should_bind(&dd, agl_shell_desktop_interface.name, 1);
 * if (ret)
 * 	return ret;
 *
 * agl_shell_setup(&dd);
 *
 * agl_shell_terminate(&dd);
 */
